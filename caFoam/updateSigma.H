forAll(mesh.cells(), cellI)
{
        if( T[cellI] > Tl.value())
        {
            sigma[cellI] = interface.sigma().value();
        }
        else
        {
            sigma[cellI] = 0.0;
        }
}


