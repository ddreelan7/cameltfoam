{
   
    scalar prevRes = GREAT;
    
    surfaceScalarField kf = threePhaseProperties.kf();
 
    //rhoCp = rho*(alphaM*cM + (scalar(1)-alphaM)*cpG);
    
    //surfaceScalarField rhoCpPhi(fvc::interpolate(alphaM*cM + (scalar(1)-alphaM)*cpG)*rhoPhi);
    
    lduSolverPerformance Tp;
 
    // volScalarField thermalDamper = (rhoCp)/(rhoM*cM + rhoG*cpG);
    
    // t_ = mesh.time();
    // dimensionedScalar x_ = x0 + (vx * t_); //Actual position of the laser
    // dimensionedScalar y_ = y0 + (vy * t_);

    // volScalarField px = mesh.C().component(0);
    // volScalarField py = mesh.C().component(1);

    // kappa = alphaM*kappaM + (scalar(1)-alphaM)*kappaG;
        
    // volScalarField gaussian = Foam::exp(-2.0*(pow(x_ - px,2)+pow(y_ - py,2))/(r0*r0));

    // laserFlux = 2.0 * kappa * Power * gaussian/(M_PI*r0*r0);
    
    // Info<<"max/min(laser): "<<max(laserFlux).value()<<", "<<min(laserFlux).value()<<endl;

    // volScalarField delGradAlpha = mag(fvc::grad(alphaM))*thermalDamper;

    for(int i=0; i<nTCorrectors; i++)
    {
        Info<< "Temperature solver Iteration :"<<i<<endl;
    
        #include "source.H"
        
        fvScalarMatrix TEqn
        (
         fvm::ddt(rhoCp,T) + fvm::div(rhoCpPhi,T, "div(phi,T)")
         //- fvm::Sp(fvc::ddt(rhoCp) + fvc::div(rhoCpPhi), T)
        + fvm::Sp(SI,T) + fvc::div(rhoMPhi,gT)
         - fvm::laplacian(kf,T)
         ==  SE
         );
        
        TEqn.relax();	
    
        Tp = TEqn.solve();
    
        T.relax();

        // T.storeOldTime();
        
        #include"liquidFraction.H"
        
        prevRes = Tp.initialResidual();
        
        if (prevRes <= temperatureTol)
        {
            break;
        }
        else if (i == (nTCorrectors - 1))
        {
            Warning
            << "Temperature equation did not converge!" << endl;
        }
        
    }
    
    Info<< "min/max(T) = "
        << min(T).value() << ", " << max(T).value() << endl;

}	
