    surfaceScalarField muEff
    (
        "muEff",
        threePhaseProperties.muf()
      + fvc::interpolate(rho*turbulence->nut())
    );

    dimensionedScalar deltaN = 1e-8/pow(average(mesh.V()), 1.0/3.0);

    volVectorField N = fvc::grad(alphaM)/(mag(fvc::grad(alphaM)) + deltaN);

    volScalarField damper = 2.0 * rho /(rhoM+rhoG);

    alphaL = alphaM*gT;

    //volTensorField pVapI = pVap * I;

    fvVectorMatrix UEqn
    (
        fvm::ddt(rho, U)
      + fvm::div(rhoPhi, U)
      - fvm::laplacian(muEff, U)
      - (fvc::grad(U) & fvc::grad(muEff))  - fvm::Sp(A,U)
      - (DSigbyDT * (fvc::grad(T) - N*(N & fvc::grad(T))) /*+ (N * (pVap * (I & N)))*/) * mag(fvc::grad(alphaM)) * damper
      //- fvc::div(muEff*(fvc::interpolate(dev(fvc::grad(U))) & mesh.Sf()))
    == -Foam::max(alphaM * gT * betaM * (T-Tl),0.0) * g * rhoM
    );

    UEqn.relax();

    if (pimple.momentumPredictor())
    {
        solve
        (
            UEqn
         ==
            fvc::reconstruct
            (
              /*- fvc::interpolate(rho * betaEff*(T-TRef))*(g & mesh.Sf())
                + */(
                 fvc::interpolate(interface.sigmaK())*fvc::snGrad(alphaM) * fvc::interpolate(damper)
                  - ghf*fvc::snGrad(rho)
                  - fvc::snGrad(pd)
                 )*mesh.magSf()
            )
        );
    }
