// Set interface cells between liquid and solid

forAll(sL, i){
//bool Foam::decentredSquare::checkLiqNbrs(List<decentredSquare>& sL, const word& nbrType){
    if( sL[i].state() == 2){
        bool liqNbr = sL[i].checkLiqNbrs(sL, neighbourhoodType);

        if (liqNbr)
        {
            sL[i].setState(1);
            sL[i].setBackup(true);
            // Need to set nSq
        }
    }
}