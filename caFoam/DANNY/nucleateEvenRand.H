    
Info << "Nucleating pseudo random" << endl;

    //- Random uniform nucleation
    scalar sideBufferNuc = 0.1;

    Random obj(1);

    //Info << "Lx:\t" << meshLx << "\tLy:\t" << meshLy << "\tLz:\t" << meshLz << endl;

    for (int i = 0; i < numberNucSites; ++i)
    {
        scalar x = (sideBufferNuc * meshLx) +  obj.scalar01() * (1 - (2 * sideBufferNuc ))*meshLx;
        scalar y = (sideBufferNuc * meshLy) +  obj.scalar01() * (1 - (2 * sideBufferNuc ))*meshLy;
        scalar z = (sideBufferNuc * meshLz) +  obj.scalar01() * (1 - (2 * sideBufferNuc ))*meshLz;

        Foam::vector position = Foam::vector(x,y,z);
        label cellI = mesh.findCell(position);

        //Info << "Rx:\t" << x << "\tRy:\t" << y << "\tRz:\t" << z << endl;

        // scalar thetaX = obj.scalar01() * 0.785398;


        if (
                (cellI > 0)
            &&  (cellI < mesh.nCells())
            )
        {

        Foam::vector theta (obj.scalar01() * 0.785398, obj.scalar01() * 0.785398, obj.scalar01() * 0.785398 );

        sL[cellI].nucleate(sL, mesh, cellI, theta, Vtype, NucleationCount, runTime.value());
        NucleationCount++;

        NucleationOrder[cellI] = NucleationCount;
        Info << "Nucleating cell " << cellI << "\tstate: " << sL[cellI].state() << endl;
        }

        else {
            FatalError << "Point " << position << "is outside of the domain" << abort(FatalError);
        }

    }

    nucleateEvenRand = false;
    growthStarted = true;