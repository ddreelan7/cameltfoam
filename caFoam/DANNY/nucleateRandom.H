Info  << "Finding nucleation probability.\tAvg undercooling = " << gAverage(Tu) << endl;


// Grain density increase (sites / m3) per change of undercooling, is given by
    
    forAll (sL, cellI){
        // Info << "CellI: " << cellI;
    	// Finding dndT
    	scalar dndT_expComp = Foam::exp( -0.5 * Foam::pow( (Tu[cellI] - Tu_mn.value())  / Tu_sd.value(), 2) );
    	
        // Info << "\tdndT_expComp: " << dndT_expComp;
    	dndT[cellI] = (n_max.value() / (sqrt2pi * Tu_sd.value()) ) * dndT_expComp;
    	
        // Info << "\tdndT = " << dndT[cellI];
    	// Finding nucleation density change by trapezoidal rule
    	scalar dn = (Tu[cellI] - Tu.oldTime()[cellI]) * ( (dndT[cellI] + dndT.oldTime()[cellI]) / Foam::scalar(2) );
    	
        // Info << "\tdn = " << dn << endl;

        // Info << "Tu[cellI]\t" << Tu[cellI]
        //     << "\t\tTu.oldTime()[cellI]\t" << Tu.oldTime()[cellI] << endl << endl;

        // Info << "T[cellI]\t" << T[cellI]
        //     << "\t\tT.oldTime()[cellI]\t" << T.oldTime()[cellI] << endl << endl;

    	// Setting nucleation probability field
    	pn[cellI] += dn * mesh.V()[cellI];
    	
    	if (pn[cellI] < 0){
    	    pn[cellI] = 0;
    	}
    	
    	if (pn[cellI] > 1){
    	    pn[cellI] = 1;
    	}
    	
    	
    	if	(
    	 			(pn[cellI] < 0.0)
    		 ||	(pn[cellI] > 1.0)
    		 	){
				FatalError << "WARNING!!! Nucleation probability for cell " << cellI << "\texceeds 0 or 1: " << pn[cellI] << abort(FatalError);
			}
			
			// Setting probability for nucleation in solid cells to zero
			if ( sL[cellI].state() != 0){
				pn[cellI] = 0;
			}
    	 	
    }

    	Info    << "Max:\t" << gMax(pn)
    	        << "\tMin:\t" << gMin(pn)
                << "\tExpected new grains : " << gAverage(pn) *  mesh.nCells();
    	        // << endl;

// now we have set the probability field for nucleation (pn)

// Find total number of cells which should be nucleated in the timestep,
// Just as a check...

//# 	include "getLiquidVolume.H"

Foam::scalar newGrainTS = 0.0;
 
forAll(sL, cellI){
	newGrainTS += pn[cellI] * mesh.V()[cellI];
}


// need to cycle through all cells and nucleate a cell if
// pn is greater than a random number (0 to 1)

	// Random obj(1); // set up random object
	
	scalar nucThisTS = 0.0;

    Info << "Time: " << runTime.value();

forAll (sL, cellI){

	// const scalar randNumber(obj.scalar01()); // generate random 0 to 1
	scalar r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

	if (   (pn[cellI] > r)
        && (sL[cellI].state() == 0)
        ) {
		// Cell is to be nucleated
		// scalar eulerRand = obj.scalar01() * 0.7853981634; // random between 0 and 45 degrees
		vector eulerAngle  = vector(  (static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) * 0.7853981634,
                                      (static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) * 0.7853981634,
                                      (static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) * 0.7853981634);  
        // Info << "EULER " << eulerAngle << endl;
		// Nucleate that cell
		NucleationCount++;
		sL[cellI].nucleate(sL, mesh, cellI, eulerAngle, Vtype, NucleationCount, runTime.value());
        growthStarted = true;
		// Info << "\nNucleated: Ldia = " << sL[cellI].Ldia(0) << endl;
		// Info << "\n\nNUCLEATED " << cellI << " state now: " << sL[cellI].state() << "\teuler "  << sL[cellI].eulerAngle() << endl;
        state[cellI] = sL[cellI].state();
        eulerX[cellI] = sL[cellI].eulerAngle(0);
        eulerY[cellI] = sL[cellI].eulerAngle(1);
        eulerZ[cellI] = sL[cellI].eulerAngle(2);

		nucThisTS += 1.0;
	}
}
        Info << "\t" << nucThisTS << " grains nucleated" << endl;

/*
Info 	<< "Number of grains that should have been nucleated:\t" << newGrainsTS << endl
			<< "Number of grains actually nucleated:\t" << nucThisTS << endl
			<< "Difference:\t" << newGrainsTS - nucThis TS << endl << endl;




//Info << numberNuc << " sites were randomly nucleated this timestep, " << endl << NucleationCount << " in total." << endl;

*/



