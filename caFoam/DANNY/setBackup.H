Info << "Setting backup switch for interface cells and their neighbours" << endl;

// Remove all backup switches
forAll(sL, i){
    sL[i].setBackup(false);
}


// Set interface cells, and their neighbours to backup
forAll(sL, i){
    if (sL[i].state() == 1)
    {
        sL[i].setBackup(true);
        
        List<label> nbrs = sL[i].mNbrs();
        forAll(nbrs, j){
            if (sL[nbrs[j]].backup() == false)
            {
                sL[nbrs[j]].setBackup(true);
            }        
        }
    }
}

backupCount = 0;


forAll(sL, i){
    if (sL[i].backup())
    {
        backupCount++;
    }
}



sL_backup = List<decentredSquare> (backupCount);

backupIndex = 0;

forAll(sL, i){
    if (sL[i].backup() == true)
    {
        sL_backup[backupIndex] = sL[i];
        // if (sL_backup[backupIndex].cell() > mesh.nCells())
        // {
        //     FatalError << "" << abort(FatalError);
        // }
        backupIndex++;
    }
}



Info << "Storing old values of fields" << endl;

T.storeOldTime();


