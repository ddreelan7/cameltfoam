// NUCLEATING CELLS BY NEW METHOD

    forAll(nuc.cellI_list, i){
    
        if( !nuc.complete[i] ){
    
            label cellI = nuc.cellI_list[i];
        
            if( cellI != -1 ){
                if ( 
                        ( (Tl.value() - T[cellI]) >= nucUndercooling.value() )
                    &&  (sL[cellI].state() == 0) 
                    ){
		  NucleationCount++;
		  sL[cellI].nucleate(sL, mesh, cellI, nuc.ABC_list[i], Vtype, NucleationCount, runTime.value());
		            nuc.complete[i] = true;
		            growthStarted = true;
		            //nucleationCount++;
		        }
            }
        }
    }
