Info << "Updating all Decentred Squares after growth and nucleation" << endl;

Switch revertTime = false;
forAll (sL, i){

    // label state = sL[i].state();

    // // if(state != 0 ){
    // // Info << "Cell " << i << " is state " << sL[i].state() << endl; 
    // // }

    // Info << "Cell " << i << " is state " << sL[i].state() << endl;

    if (sL[i].state() == 1){        
        //sL[i].setFs(alphaS[i]);
        scalar vdt = Vmag[i] * runTime.deltaTValue();
        // Info << "vdt: " << vdt <<"\tdm: " << dm << nl << endl;
        if(vdt > dm){
            Info << "Growth larger than mesh size: " << vdt << "\tdm: " << dm << nl 
                <<"Current dt: "<< runTime.deltaTValue() << "\tNeed dt < " << dm/Vmag[i] << " s" << endl;//abort(FatalError);
            Info << "\n\n\n\t\tWARNING: timestep should be reverted." << endl;
            revertTime = true;
            break;
        }

        // if (
        //         (Vtype == "solute")
        //     &&  (vdt == 0)
        //     )
        // {
        //     vdt = dm * runTime.deltaTValue() * relaxDeltaT; // Just for testing
        // }
       
        // Info << "Before grow" << endl;
        sL[i].grow(vdt);
        // Info << "After grow" << endl;
        // Info << "\nLdia/dm: " << sL[i].Ldia(0) / dm << endl;
        // Info << "Ldia: " << sL[i].Ldia(0) << endl;

        // Info << "v: " << sL[i].getVertices(0) << endl;  
	}
}


// if (revertTime)
// {  
//     // FatalError << "revertTime trigger successful" << abort(FatalError); 
//     #   include "revertTime.H"
// }
        // Info << "After all grow" << endl;


// For cell edge approach

if ( capturePoint == "cellEdge" ){
    // Info << "cellEdge capture" << endl;

    forAll( sL, i){
    //    Info << "Cell being checked: " << i << endl;
        if ( 
                (sL[i].state() == 1 ) // Interface
          // ||   (sL[i].state() == 12)     
                
           ){

            // Info << "interface cell" << endl;

            // Info << "sL[i].squareCentre(): " << sL[i].squareCentre() << endl;

            forAll(sL[i].squareCentre(), s){
                 // Info << "s = " << s << endl;
                List<vector> v = sL[i].getVertices(s);
                 // Info << "After getVertices" << endl;
                
                forAll( v, j ){
                    // Info << "j = " << j << " before findcell" << endl;
                    label foundCellI = mesh.findCell( v[j] );
                     // Info << "foundCellI:\t" << foundCellI << endl; 
            
                // Info << "Cell found: " << foundCellI << "\tCellI: " << i << endl;
                    if (multiSquare){
                        if ( 
                                (
                                    (foundCellI != sL[i].cellI() ) // cell is not parent cell
                                 && (foundCellI != -1)
                                )
                             && ( 
                                    (sL[foundCellI].state() == 0) // cell is liquid
                                 || (sL[foundCellI].state() == 11) // newly formed interface
                                )
                            ){

                            // Info << "Cell " << foundCellI << " is state " << sL[foundCellI].state() << " being captured by " << i << endl;
                            // foundCellI is being captured by i, of square s, by vertice j
                            sL[foundCellI].captureV(sL[i], s, j);
                            // Info << "i: " << i << "\tCellI: " << sL[i].cellI() << endl;
                        }

                    }

                    else{
                        if ( 
                                (foundCellI != sL[i].cellI() ) // cell is not parent cell
                             && ( 
                                    (sL[foundCellI].state() == 0) // cell is liquid
                                )
                            ){

                            // Info << "Cell " << foundCellI << " is state " << sL[foundCellI].state() << " being captured by " << i << endl;

                            if( sL[foundCellI].nbrCap(i) == false){
                                sL[foundCellI].captureV(sL[i], s, j);    
                            }
                        }

                    }

                }
            }
        }
    }
}

Info << "Counting interface cells..." << endl;

    label OLDcountLiq = countLiq;

    label OLDcountInt = countInt;
    label OLDcountIntNew = countIntNew;

    label OLDcountSol = countSol;
    label OLDmaxN = maxN;

    countLiq = 0;

    countInt = 0;
    countIntNew = 0;

    countSol = 0;
    maxN = 0;

    forAll(sL, i){


        if(
                (sL[i].state()==1)
            &&  (sL[i].nSq() > maxN)
            ){

            maxN = sL[i].nSq();

            if (maxN > 1)
            {
                FatalError << "maxN > 1 " << abort(FatalError);
            }
        }

        label stateCell = sL[i].state();

        if(stateCell == 1){
            countInt++;
            // Info << "\nLdia/dm: " << sL[i].Ldia(0) /dm;
        }
        else if (stateCell == 11){
            // Info << "Cell " << i << " capd by:\n";
            // List<label> capNbrs = sL[i].parentCell();
            // forAll(capNbrs, j){
            //     Info << capNbrs[i] << endl;
            // }
            // Info << endl;
            countIntNew++;
            sL[i].setState(1);
        }

        else if (stateCell == 2){
            countSol++;
        }

        else if (stateCell == 0)
        {
            countLiq++;
        }
    }

// Info << "Changing cells to solid " << endl;

// Info << "neighbourhoodType: " << neighbourhoodType << endl;

forAll(sL, i){
    if ( 
            (sL[i].state() == 1 )
        &&  (!sL[i].checkLiqNbrs(sL, neighbourhoodType))
        ){
            sL[i].setState(2);
    }
}

Info << "Interface cell count = " << countInt << "\tNEW: " << countIntNew << "\tmax N = " << maxN << 
    "\nSolid: " << countSol << "\tLiquid: " << countLiq << "\nCompletion: " << ( (double) countSol / (double) mesh.nCells() )*100.0 
        << "% (solid)\t" << ( (double) countInt / (double) mesh.nCells() )*100.0 << "% (interface)" << endl;


//- Checking if these counts do not change between timesteps.

    label DcountLiq = countLiq - OLDcountLiq;

    label DcountInt = countInt - OLDcountInt;
    label DcountIntNew = countIntNew - OLDcountIntNew;

    label DcountSol = countSol - OLDcountSol;
    label DmaxN = maxN - OLDmaxN;

    Info << "\nDcountInt: " << DcountInt << "\tDcountIntNew: " << DcountIntNew << "\tDcountSol: " << DcountSol << endl;

    if (    
                (DcountInt      == 0)
            &&  (DcountSol      == 0)
            &&  (DcountInt      == 0)
            &&  (DcountIntNew   == 0)
            &&  (countSol       != 0)
        )
    {
        Info << "\n\nWARNING: States not changing\n\n" << endl;
        // #   include "printSquares.H"

        //FatalError << "ABORTING\n" << abort(FatalError);
    }

