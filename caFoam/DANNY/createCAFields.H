    Info<< "Creating state field (liquid[0], interface[1] or solid[2]) (state)\n" << endl;

    volScalarField state
    (
        IOobject
        (
            "state",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh,
    0.0
    );

    volScalarField Tu
    (
        IOobject
        (
            "Tu",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        Tl - T
    );

//		typedef Field <decentredSquare> dcsa ( mesh );

    Info<< "Reading orientation field (Orient)\n" << endl;
    volVectorField eulerAngle
    (
        IOobject
        (
            "eulerAngle",
            runTime.timeName(),
            mesh,
            //IOobject::NO_READ,
            IOobject::READ_IF_PRESENT,            
        	IOobject::NO_WRITE
        ),
        mesh,
		Foam::vector(-1,-1,-1)
    );
    
    // Half diagonal length of line (1D), square (2D) or octahedron (3D)
        volScalarField Ldia
        (
            IOobject
            (
                "Ldia",
                runTime.timeName(),
                mesh,
                IOobject::NO_READ,
                IOobject::NO_WRITE
            ),
            mesh,
			Foam::scalar(0.0)
        );

        volScalarField LdiaDm
        (
            IOobject
            (
                "LdiaDm",
                runTime.timeName(),
                mesh,
                IOobject::NO_READ,
                IOobject::AUTO_WRITE
            ),
            mesh,
            Foam::scalar(0.0)
        );

    volScalarField Vmag
    (
        IOobject
        (
            "Vmag",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mesh,
        Vmag_user
    );

        volScalarField vUnitX
        (
            IOobject
            (
                "vUnitX",
                runTime.timeName(),
                mesh,
                IOobject::NO_READ,
                IOobject::NO_WRITE
            ),
            mesh,
            Foam::scalar(0.0)
        );

        volScalarField vUnitY
        (
            IOobject
            (
                "vUnitY",
                runTime.timeName(),
                mesh,
                IOobject::NO_READ,
                IOobject::NO_WRITE
            ),
            mesh,
            Foam::scalar(0.0)
        );

        volScalarField vUnitZ
        (
            IOobject
            (
                "vUnitZ",
                runTime.timeName(),
                mesh,
                IOobject::NO_READ,
                IOobject::NO_WRITE
            ),
            mesh,
            Foam::scalar(0.0)
        );
        
		// Square centre offset from cell centre
        volVectorField Csq
        (
            IOobject
            (
                "Csq",
                runTime.timeName(),
                mesh,
                IOobject::NO_READ,
                IOobject::NO_WRITE
            ),
           	mesh,
           	vector(0,0,0)
        );
        
    
  		Foam::scalar NucleationCount = Foam::scalar(0.0);
    		
        volScalarField NucleationOrder
        (
            IOobject
            (
                "NucleationOrder",
                runTime.timeName(),
                mesh,
                IOobject::NO_READ,
                IOobject::AUTO_WRITE
            ),
            mesh,
			Foam::scalar(0.0)
        );
        
        
        volScalarField eulerX
        (
            IOobject
            (
                "eulerX",
                runTime.timeName(),
                mesh,
                IOobject::READ_IF_PRESENT,
                IOobject::AUTO_WRITE
            ),
            eulerAngle.component(vector::X)
        );
        
    
        volScalarField eulerY
        (
            IOobject
            (
                "eulerY",
                runTime.timeName(),
                mesh,
                IOobject::READ_IF_PRESENT,
                IOobject::AUTO_WRITE
            ),
            eulerAngle.component(vector::Y)
        );
        
    
        volScalarField eulerZ
        (
            IOobject
            (
                "eulerZ",
                runTime.timeName(),
                mesh,
                IOobject::READ_IF_PRESENT,
                IOobject::AUTO_WRITE
            ),
            eulerAngle.component(vector::Z)
        );

    volScalarField parentSq
    (
        IOobject
        (
            "parentSq",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        ),
	mesh,
	-1.0
    );

    Info << "\nnbrOrder\n" << endl;
    volScalarField nbrOrder
    (
        IOobject
        (
            "nbrOrder",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::NO_WRITE
        ),
        mesh,
        -1.0
    );

    volScalarField backupField
    (
        IOobject
        (
            "backupField",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::NO_WRITE
        ),
        mesh,
        0.0
    );

    label countLiq = 0;

    label countInt = 0;
    label countIntNew = 0;

    label countSol = 0;
    label maxN = 0;
