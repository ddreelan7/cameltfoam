Info << "Setting cell states and initialising fields based on undercooling" << endl;

Info << "Tl: " << Tl.value() << endl;

forAll(sL, i){
    // Info << "Tu[" << i << "]: " << Tl.value() - T[i] << endl;

    scalar TuCell = Tl.value() - T[i];
    if ( (Tl.value() - T[i]) > 0){ // Solid
        sL[i].setState(2);
        sL[i].setEulerRandom();
        // Info << "EulerAngle[" << i << "]: " << sL[i].eulerAngle() << endl;
    }

    if( (Tl.value() - T[i]) <= 0 ){ // Positive undercooling
        sL[i].setState(0);
    }
}


#   include "setInterface.H"

#   include "updateFields.H"

// FatalError << abort(FatalError);