Info << "Melting CA cells above the liquidus" << endl;

forAll(sL, i ){
    
    if( (Tl.value() - T[i]) < 0 ){
        sL[i].melt();
        // Info << "MELT" << endl;
        // state[i] = sL[i].state();
    }
}

# include "setInterface.H"