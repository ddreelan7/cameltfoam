// Code to choose random nucleation points to start CA cells.

Info << "\nNucleating cells as specified in CAparameters\n" << endl;

IOdictionary nucPts
    (
        IOobject
        (
            "CAparameters",
            runTime.constant(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::NO_WRITE
        )
    );


struct nucList {

DynamicList<bool>   complete;	
DynamicList<vector> XYZ_list;
DynamicList<label> 	cellI_list;
DynamicList<vector> ABC_list;

};

nucList nuc;

//DynamicList< nucList > nucData (0);

//nuc.XYZ 		= DynamicList<vector>(0);
//nuc.CellI 	= DynamicList<label>(0);
//nuc.ABC 		= DynamicList<vector>(0);
    
if (nucPts.headerOk())
{

List<vector> xyz_init = List<vector>(nucPts.lookup("Location"));
Info << "Number of nucleation points: " << xyz_init.size() << endl;

List<vector> abc_init = List<vector>(nucPts.lookup("Orientation"));
Info << "Number of nucleation orientations: " << abc_init.size() << endl;


	
 // CHECK SO THAT SAME NUM Locations as Orientations
if (xyz_init.size() != abc_init.size())
{
	FatalError << "List of positions and orientation in nucleationInit are not of equal length." << abort(FatalError);
}


// Check units of angles
word OriUnit = nucPts.lookup("OrientationUnit");

Info<< "OrientationUnit:\t" << OriUnit << endl;

if (
			(OriUnit !=	"degrees")
	&&	(OriUnit !=	"radians")
	)
{
			FatalError << "Invalid Euler Angle units specified, needs to be:\ndegrees\tOR\nradians" << abort(FatalError);
}

// If degrees, convert to radians

if (OriUnit == "degrees")
{
	// Need to convert to radians
	forAll(abc_init, i)
	{
		abc_init[i] = abc_init[i] * 0.01745329252;
	}
}


forAll(xyz_init, i)
{
	
	label cellI = mesh.findCell(xyz_init[i]);
	if (cellI == -1)
	{
		FatalError << "Nucleation point " << xyz_init[i] <<" from constant/nucleationParameters is outside domain " << abort(FatalError);
	}
		else {
	//nuc.time = runTime.deltaTValue();
	nuc.complete.append(false);
	nuc.ABC_list.append(abc_init[i]);	
	nuc.cellI_list.append(cellI);
	}
	
}
}
