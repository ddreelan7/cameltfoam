Info << "Printing squares for remaining interface cells" << endl;

forAll (sL, i){
    if (sL[i].state() == 1){
        label nSq = sL[i].nSq();
        Info << "\nCELL " << sL[i].cellI() << " has " << nSq << " squares" << endl;

        for( int j = 0; j < nSq; j++){
            List<vector> verts = sL[i].getVertices(j);
            Info << "~~~~~~ Square " << j+1 << "~~~~~~" << endl
                << "Ldia:\t\t" << sL[i].Ldia(j) << endl
                << "SqCen:\t\t" << sL[i].squareCentre(j) << endl;

                forAll(verts, k){

                    label foundCellI = mesh.findCell(verts[k]);
                    Info << "V[" << k << "]:\t\t" << verts[k] << "\t\tCellI: " << foundCellI;
                    if (foundCellI != -1)
                    {
                        Info << "\tState: " << sL[foundCellI].state();
                        if (sL[foundCellI].state() == 1){
                            //- Capture Cell
                            //         void captureV(const decentredSquare& parSq, const label& sqI, const label& indexV);
                           // sL[foundCellI].captureV(sL[i], j, k);
                        }
                    }
                    Info << endl;
                }
                // Info

                // << "\nV0:\t\t" << verts[0]  << "\t\tCellI: " << mesh.findCell(verts[0]) << endl;
                // << "V1:\t\t" << verts[1]    << "\t\tCellI: " << mesh.findCell(verts[1]) << endl
                // << "V2:\t\t" << verts[2]    << "\t\tCellI: " << mesh.findCell(verts[2]) << endl
                // << "V3:\t\t" << verts[3]    << "\t\tCellI: " << mesh.findCell(verts[3]) << endl
                // << "V4:\t\t" << verts[4]    << "\t\tCellI: " << mesh.findCell(verts[4]) << endl
                // << "V5:\t\t" << verts[5]    << "\t\tCellI: " << mesh.findCell(verts[5]) << endl;

        }
    }

}