Info << "Finding growth velocity based on local undercooling" << endl;

forAll( Tu, cellI){

    scalar expPart = Foam::pow(Tu[cellI], nExp);
    Vmag[cellI] = C2 * expPart;
    
    if( Tu[cellI] < 0){
        Vmag[cellI] *= -1;
    }
}

forAll(sL, i){
    if(     (sL[i].state() != 1)
        &&  (sL[i].state() != 11)
        ){
        Vmag[i] = 0;
    }
}

Info << "maxV: " << gMax(Vmag) << "\tavgV: " << gAverage(Vmag) << "\tminV: " << gMin(Vmag) << endl;
