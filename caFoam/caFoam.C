/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | foam-extend: Open Source CFD
   \\    /   O peration     | Version:     4.0
    \\  /    A nd           | Web:         http://www.foam-extend.org
     \\/     M anipulation  | For copyright notice see file Copyright
-------------------------------------------------------------------------------
License
    This file is part of foam-extend.

    foam-extend is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    foam-extend is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with foam-extend.  If not, see <http://www.gnu.org/licenses/>.

Application
    laserMeltFoam

Description
    Solver for 2 incompressible, isothermal immiscible fluids and using a VOF
    (volume of fluid) phase-fraction based interface capturing approach and solidification of liquid based on Voller method.

    The momentum and other fluid properties are of the "mixture" and a single
    momentum equation is solved.

    Turbulence modelling is generic, i.e.  laminar, RAS or LES may be selected.
 
Author
    Gowthaman Parivendhan, UCD
 
        "When I wrote this, only God and I understood what I was doing
        Now, only God knows"
                                                    -Karl Weierstrass
\*---------------------------------------------------------------------------*/


#include "fvCFD.H"
#include "MULES.H"
#include "subCycle.H"
#include "interfaceProperties.H"
#include "laserThreePhaseMixture.H"
#include "turbulenceModel.H"
#include "pimpleControl.H"

#include "liquidFractionFunctions.H"

#include "preIncludeDanny.H"
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
#   include "setRootCase.H"
#   include "createTime.H"
#   include "createMesh.H"
// 
    pimpleControl pimple(mesh);

#   include "readGravitationalAcceleration.H"
#   include "initContinuityErrs.H"
#   include "createFields.H"
//#   include "createLaser.H"
#   include "createTimeControls.H"
#   include "correctPhi.H"
#   include "CourantNo.H"
#   include "setInitialDeltaT.H"

#   include "includeDanny.H"

//#   include "surfaceNucleationEveryCell.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nStarting time loop\n" << endl;

//     Info << "TESTING FPR" << endl;

//     scalar timeToNextWrite (1);

//     //scalar dt (4.38269447262e-10);
//     scalar dt (4e-10);

// //    scalar result = label( timeToNextWrite/dt - SMALL) + 1;
//     scalar result = timeToNextWrite/dt + 1;
//     Info << "result: " << result << endl;

//     label result2 =     label( timeToNextWrite/dt ) + 1;
//     Info << "result2: " << result2 << endl;

//     label result3 = label (result);
//     Info << "result3: " << result2 << endl;

//     label labelTest = label(2210000000);
//     Info << "labelTest: " << labelTest << endl;

//     abort();

    scalar originalDeltaT = runTime.deltaTValue();

    while (runTime.run())
    {
        //make a backup of the current state
        if (revertTimeOnFailure)
        {
            TimeState tSCurrent(runTime);
        }

        // if (runTime.value() >= 5)
        // {   
        //     growthStarted = true;
        // }

        runTime++;
        
        Info<< "Time = " << runTime.timeName() << "\tdeltaT: " << runTime.deltaTValue() << nl << endl; 

        if (isothermalT){
                #   include "TEqn_isothermal.H"
        }

        else{

            while (pimple.loop())
            {
                    #   include "TEqn.H"                
            }
        }

        if (useCA)
            {
                #   include "CAalgorithm.H"
            }
            
        #   include "updateFields.H"

        Info<< "ExecutionTime = " << runTime.elapsedCpuTime() << " s"
            << "  ClockTime = " << runTime.elapsedClockTime() << " s"
            << nl << endl;

        runTime.write();
    }
    Info<< "End\n" << endl;

    return 0;
}


// ************************************************************************* //
