label nAlphaCorr
(
    readLabel(pimple.dict().lookup("nAlphaCorr"))
);

label nAlphaSubCycles
(
    readLabel(pimple.dict().lookup("nAlphaSubCycles"))
);

if (nAlphaSubCycles > 1)
{
    dimensionedScalar totalDeltaT = runTime.deltaT();
    surfaceScalarField rhoPhiSum = 0.0*rhoPhi;
    surfaceScalarField rhoCpPhiSum = 0.0 * rhoCpPhi;
    surfaceScalarField rhoMPhiSum = 0.0*rhoMPhi;

    for
    (
        subCycle<volScalarField> alphaSubCycle(alphaM, nAlphaSubCycles);
        !(++alphaSubCycle).end();
    )
    {
#       include "alphaEqn.H"
        rhoPhiSum += (runTime.deltaT()/totalDeltaT)*rhoPhi;
	rhoCpPhiSum += (runTime.deltaT()/totalDeltaT)* rhoCpPhi;
        rhoMPhiSum += (runTime.deltaT()/totalDeltaT)*rhoMPhi;
    }

    rhoPhi = rhoPhiSum;
    rhoMPhi = rhoMPhiSum;
    rhoCpPhi = rhoCpPhiSum;
}
else
{
#       include "alphaEqn.H"
}

interface.correct();

//Update the rho and cp for material
rho == alphaM*rhoM + (scalar(1) - alphaM)*rhoG;
cM=gT*cpL + (scalar(1)-gT)*cpS;

//rhok = rho * (1.0 - betaEff*(T - TRef));
//betaEff = alphaM*betaM + (scalar(1) - alphaM)*betaG;
//Update zeta - Refer Appendix A in the report
rhoCp == alphaM* rhoM *cM + (scalar(1) - alphaM)*rhoG*cpG;

//Update momentum sink term
A == -1.0 * C * alphaM * (Foam::pow(scalar(1)-gT,2))/(Foam::pow(gT,3)+SMALL);

