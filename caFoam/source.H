 deltaH = rhoM*(cpL - cpS)*T + rhoM*Hf;


//Implicit Term coefficient
SI = (/*((alphaM - alphaM.oldTime()) * (rhoM*cM - rhoG*cpG)) +*/ ((gT - gT.oldTime()) * alphaM * rhoM * (cpL - cpS)))/runTime.deltaT();

//Explicit Term - Divergence part included in TEqn
SE = ((alphaM.oldTime() * gT.oldTime()) - (alphaM * gT)) * rhoM * Hf / runTime.deltaT();
//SE = ((alphaM.oldTime() * gT.oldTime()) - (alphaM * gT)) * rhoM * Hf / runTime.deltaT();
//SE = - Hf *(fvc::div(rhoMPhi,gT) + fvc::ddt(rhoM*alphaM,gT));

//Heat Equation source term
 //Sh = deltaH*(gT.oldTime() - gT) / runTime.deltaT();
