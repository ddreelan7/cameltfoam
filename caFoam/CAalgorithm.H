// Info << "CA Algorithm" << endl;

if (nucleateRandom)
{
    #   include "nucleateRandom.H"
}

Info << "Getting growth velocity based on undercooling" << endl;
#   include "getVkinetics.H"

if (growthStarted)
{
    Info << "Adjusting deltaT based on maximum growth velocity" << endl;
    #   include "adjustDeltaT.H"

    // #   include "revertTime.H"

    // FatalError << "revertTime done" << endl;
}

Info << "Calculating undercooling" << endl;
#   include "calcTu.H"

if (CAmelt)
{
    Info << "Melting CA cells based on thermal field" << endl;
    #   include "meltCA.H"
}

// Info << "Nucleating new cells" << endl;
// #   include "nucleate.H"

if (revertTimeOnFailure)
{
    Info << "Making backup of fields in case timestep needs to be reverted." << endl;
    #   include "setBackup.H"
}

Info << "Running CA algorithm" << endl;
#   include "updateDCSAlist.H"

// FatalError << abort(FatalError);
