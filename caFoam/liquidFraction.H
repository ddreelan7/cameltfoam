{
/*----------------------------OLD CORRECTION----------------------------------*/

    //correction for gT = 1
    //update deltaH with new temperature
    deltaH = rhoM*(cpL - cpS)*T + rhoM*Hf;

    //correction based on old methotdology
    volScalarField corr =
        (runTime.deltaT()*TEqn.A()*(T - invLiquidFraction(gT, Tl, Ts)))
        / (deltaH + runTime.deltaT()*TEqn.A()*invDFbyDT(T, Tl, Ts));

    Info << "corr, max: " << gMax(corr)
         << " corr, min: " << gMin(corr)
         << " corr, avg: " << gAverage(corr)
         << endl;

    Info << "After corr: " << endl;

    gT = gT + relax*corr;

/*-------------------NEW METHOD WHICH IS USING BOTH CORRECTIONS---------------*/
/*
    //in order to extract values of diagonal members for T equation
    volScalarField diagT = TEqn.A();
    
    //loop over all cells
    forAll(mesh.cells(), cellI)
    {
        //if current gT is smaller then 1.0 correct it with new method
        if ( gT[cellI] < (1.0 - SMALL))
        {
            gT[cellI] = gT[cellI] +
            DFbyDT(T[cellI], Tl, Ts)*T[cellI]
            - DFbyDT(T[cellI], Tl, Ts)
            *invLiquidFraction(gT[cellI], Tl, Ts);
        }
        //otherwise use old method
        else
        {
            
            scalar corr = 0.0;
            
            //update deltaH with new temperature
            deltaH[cellI] = (gT[cellI]*rhoL.value() + (scalar(1)-gT[cellI])*rhoS.value())*(cpL.value() - cpS.value())*T[cellI] +
            (gT[cellI]*rhoL.value() + (scalar(1)-gT[cellI])*rhoS.value())*Hf.value();
            
            //calculate correction Field
            corr=
            (runTime.deltaT().value()*diagT[cellI]*(T[cellI] -
                                                    invLiquidFraction(gT[cellI], Tl, Ts)))
            / (deltaH[cellI] +
               runTime.deltaT().value()*diagT[cellI]*invDFbyDT(T[cellI], Tl, Ts));
            
            //update gT
            gT[cellI] = gT[cellI] + relax*corr;
        }
    }*/
    
    
    forAll(mesh.cells(), cellI)
    {
        if( gT[cellI] > 1.0)
        {
            gT[cellI] = 1.0;
        }
        else if( gT[cellI] < 0.0)
        {
            gT[cellI] = 0.0;
        }
    }
    
    //necessary to trunctate gT on boundary field
    gT.correctBoundaryConditions();
    
    Info << "After gT update:\t" << "  Min(gT) = " << min(gT).value()
    << "  Max(gT) = " << max(gT).value() << endl;
}

