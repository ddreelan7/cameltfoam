# Python Script to set view direction in paraview
# The goal here is verify that crystal orientation is if fact conserved, it looks like it is
 # Need to read:
# 1 - Nucleation Centre
# 2 - Orientation of <100>
# 3 - set zoom a certain distance away from centre
# %   Camera settings:

import numpy as np
file= open("centreO", "r")

contents = file.readlines()

eulerRaw=contents[0]
eulerRaw = eulerRaw.rstrip()
eulerRaw = eulerRaw.replace('(','')
eulerRaw = eulerRaw.replace(')','')
eulerRaw = eulerRaw.split(' ')
npEuler = np.array(eulerRaw)
euler = npEuler.astype(np.float)

focalRaw=contents[1]
focalRaw = focalRaw.rstrip()
focalRaw = focalRaw.replace('(','')
focalRaw = focalRaw.replace(')','')
focalRaw = focalRaw.split(' ')
npFocal = np.array(focalRaw)
focal = npFocal.astype(np.float)

positionRaw=contents[2]
positionRaw = positionRaw.rstrip()
positionRaw = positionRaw.replace('(','')
positionRaw = positionRaw.replace(')','')
positionRaw = positionRaw.split(' ')
npPosition = np.array(positionRaw)

position = npPosition.astype(np.float)

positionScale = 0.01
position *= positionScale;

d = position - focal
dUnit = d / np.linalg.norm(d, ord=1 )
position = (positionScale * dUnit) + focal

upRaw   =   contents[3]
upRaw = upRaw.rstrip()
upRaw = upRaw.replace('(','')
upRaw = upRaw.replace(')','')
upRaw = upRaw.split(' ')
npUp = np.array(upRaw)
up = npUp.astype(np.float)
# print(up)
# # yawAngle = euler[0]
# yawAngle = euler[0] *  57.2958
# pitchAngle = euler[1] *  57.2958
# rollAngle = euler[2] *  57.2958

camera = GetActiveCamera()
camera.SetPosition(position[0], position[1], position[2])
camera.SetFocalPoint(focal)
camera.SetViewUp(up)
camera.SetFocalDistance(0.00001)
# camera.Yaw(yawAngle)
# camera.Pitch(pitchAngle)
# camera.Roll(rollAngle)
Render()

point1 = (0,0,0)
point2 = (1,1,1)
line1=Line(point1, point2)

Show()













