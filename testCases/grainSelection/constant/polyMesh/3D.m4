/*--------------------------------*- C++ -*----------------------------------* \
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM Extend Project: Open Source CFD        |
|  \\    /   O peration     | Version:  1.6-ext                               |
|   \\  /    A nd           | Web:      www.extend-project.de                 |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant/polyMesh";
    object      blockMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// Model Description
// 1m x 1m 2D square (bottom left corner, with symm planes)

// Setup m4 stuff
changecom(//)changequote([,])
define(calc, [esyscmd(perl -e 'printf ($1)')])

// define geometry in m
define(Lc, 10e-3)
define(Lx, Lc)  // side length
define(Ly, Lc) // y depth
define(Lz, Lc) // y depth

define(mc, 100)

define(mx, mc) // divs in x
define(my, mc) // divs in y
define(mz, mc)
// start of blockMeshDict

convertToMeters 1;

vertices
(
    // Front
	(0 0 0)                	// 0
	(Lx 0 0)			// 1
	(Lx Ly 0)			// 2
	(0 Ly 0)			// 3	

    // Back
	(0 0 Lz)                	// 4
	(Lx 0 Lz)			// 5
	(Lx Ly Lz)			// 6
	(0 Ly Lz)			// 7
);

blocks
(
    hex (0 1 2 3 4 5 6 7) (mx my mz) simpleGrading (1 1 1)
);

edges
(
);


boundary
(
	frontAndBack
	{
		type wall;
		faces
		(
			(0 1 2 3)
      (4 5 6 7)
		);
	}
	
	left
	{
		type wall;
		faces
		(
			(0 4 7 3)
		);
	}
	
	bottom
	{
		type wall;
		faces
		(
			(0 1 5 4)
		);
	}
	
	right
	{
		type wall;
		faces
		(
			(1 5 6 2)
		);
	}
	
	top
	{
		type wall;
		faces
		(
			(3 2 6 7)
		);
	}	
);

mergePatchPairs
(
);
