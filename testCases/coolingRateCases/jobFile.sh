#!/bin/bash

# Slurm flags
#SBATCH --job-name=1deg-Multi
# speficity number of nodes 
#SBATCH -N 1

# specify number of tasks/cores per node required
#SBATCH --ntasks-per-node 1

# specify the walltime e.g 20 mins
#SBATCH -t 00:05:00

# set to email at start,end and failed jobs
#SBATCH --mail-type=ALL
#SBATCH --mail-user=daniel.dreelan@ucdconnect.ie

# run from current directory
# cd /home/people/13316126/foam/13316126-4.0/run/PHD/Al4Cu_3D_10deg
cd $SLURM_SUBMIT_DIR

# command to use
source ~/foam/foam-extend-4.0/etc/bashrc

blockMesh &> log.blockMesh
caFoam &> log.1deg
