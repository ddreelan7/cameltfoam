/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | foam-extend: Open Source CFD
   \\    /   O peration     | Version:     4.0
    \\  /    A nd           | Web:         http://www.foam-extend.org
     \\/     M anipulation  | For copyright notice see file Copyright
-------------------------------------------------------------------------------
License
    This file is part of foam-extend.

    foam-extend is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    foam-extend is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with foam-extend.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "laserThreePhaseMixture.H"
#include "addToRunTimeSelectionTable.H"
#include "surfaceFields.H"
#include "fvc.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * * //

//- Calculate and return the laminar viscosity
word threePhaseMixture::getPhaseName(const word& key) const
{
    if (isDict(key))
    {
        return key;
    }
    else
    {
        return word(lookup(key));
    }
}

void threePhaseMixture::calcNu()
{
    nuModel1_->correct();
    nuModel2_->correct();

    volScalarField limitedAlphaM
    (
        "limitedAlphaM",
        min(max(alphaM_, scalar(0)), scalar(1))
    );

    volScalarField limitedgT
    (
     "limitedgT",
     min(max(gT_, scalar(0)), scalar(1))
     );
    
    // Average kinematic viscosity calculated from dynamic viscosity
    nu_ = mu()/(limitedAlphaM*rhoM_ + (scalar(1) - limitedAlphaM)*rhoG_);
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //
threePhaseMixture::threePhaseMixture
(
    const volVectorField& U,
    const surfaceScalarField& phi,
    const word& alphaMName,
    const word& gTName
)
:
    transportModel(U, phi),

    phase1Name_(getPhaseName("MaterialProperties")),
    phase2Name_(getPhaseName("GasProperties")),

    nuModel1_
    (
        viscosityModel::New
        (
            "nu1",
            subDict(phase1Name_),
            U,
            phi
        )
    ),
    nuModel2_
    (
        viscosityModel::New
        (
            "nu2",
            subDict(phase2Name_),
            U,
            phi
        )
    ),

    rhoM_("rhoM", dimensionSet(1,-3,0,0,0,0,0),nuModel1_->viscosityProperties().lookup("rhoS")),
    rhoG_("rhoG", dimensionSet(1,-3,0,0,0,0,0),nuModel2_->viscosityProperties().lookup("rhoG")),
    
    cpS_("cpS", dimensionSet(0,2,-2,-1,0,0,0),nuModel1_->viscosityProperties().lookup("cpS")),
    cpL_("cpL", dimensionSet(0,2,-2,-1,0,0,0),nuModel1_->viscosityProperties().lookup("cpL")),
    cpG_("cpG", dimensionSet(0,2,-2,-1,0,0,0),nuModel2_->viscosityProperties().lookup("cpG")),
    
    kS_("kS", dimensionSet(1,1,-3,-1,0,0,0),nuModel1_->viscosityProperties().lookup("kS")),
    kL_("kL", dimensionSet(1,1,-3,-1,0,0,0),nuModel1_->viscosityProperties().lookup("kL")),
    kG_("kG", dimensionSet(1,1,-3,-1,0,0,0),nuModel2_->viscosityProperties().lookup("kG")),
    
    U_(U),
    phi_(phi),

    alphaM_(U_.db().lookupObject<const volScalarField> (alphaMName)),
    gT_(U_.db().lookupObject<const volScalarField> (gTName)),
    
    nu_
    (
        IOobject
        (
            "nu",
            U_.time().timeName(),
            U_.db()
        ),
        U_.mesh(),
        dimensionedScalar("nu", dimensionSet(0, 2, -1, 0, 0), 0),
        calculatedFvPatchScalarField::typeName
    )
{
    calcNu();
}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

tmp<volScalarField> threePhaseMixture::rho() const
{
    volScalarField limitedAlphaM = min(max(alphaM_, scalar(0)), scalar(1));
    
    return tmp<volScalarField>
    (
        new volScalarField
        (
            "rho_threePhaseMixture",
            limitedAlphaM*rhoM_
          + (scalar(1) - limitedAlphaM)*rhoG_
        )
    );
}

tmp<volScalarField> threePhaseMixture::alphaM() const
{
    return tmp<volScalarField>
    (
     new volScalarField
     (
      "alpha.material",
      alphaM_
      )
     );
}
    
tmp<volScalarField> threePhaseMixture::mu() const
{
    volScalarField limitedAlphaM = min(max(alphaM_, scalar(0)), scalar(1));
    return tmp<volScalarField>
    (
        new volScalarField
        (
            "mu_threePhaseMixture",
            limitedAlphaM*rhoM_*nuModel1_->nu()
          + (scalar(1) - limitedAlphaM)*rhoG_*nuModel2_->nu()
        )
    );
}


tmp<surfaceScalarField> threePhaseMixture::muf() const
{
    surfaceScalarField alphaMf =
        min(max(fvc::interpolate(alphaM_), scalar(0)), scalar(1));
    
    return tmp<surfaceScalarField>
    (
        new surfaceScalarField
        (
            "muf_threePhaseMixture",
            alphaMf*rhoM_*fvc::interpolate(nuModel1_->nu())
          + (scalar(1) - alphaMf)*rhoG_*fvc::interpolate(nuModel2_->nu())
        )
    );
}


tmp<surfaceScalarField> threePhaseMixture::nuf() const
{
    surfaceScalarField alphaMf =
        min(max(fvc::interpolate(alphaM_), scalar(0)), scalar(1));
    
    return tmp<surfaceScalarField>
    (
        new surfaceScalarField
        (
            "nuf_threePhaseMixture",
            (
                alphaMf*rhoM_*fvc::interpolate(nuModel1_->nu())
              + (scalar(1) - alphaMf)*rhoG_*fvc::interpolate(nuModel2_->nu())
            )/(alphaMf*rhoM_ + (scalar(1) - alphaMf)*rhoG_)
        )
    );
}

                                                                                   
tmp<surfaceScalarField> threePhaseMixture::kf() const
{
    surfaceScalarField alphaMf =
    min(max(fvc::interpolate(alphaM_), scalar(0)), scalar(1));
    
    surfaceScalarField gTf =
    min(max(fvc::interpolate(gT_), scalar(0)), scalar(1));
    
    return tmp<surfaceScalarField>
    (
        new surfaceScalarField
        (
          "kf",
         alphaMf*(gTf * kL_ + (scalar(1) - gTf) * kS_)
         + (scalar(1) - alphaMf)*kG_
          )
         );
    }

bool threePhaseMixture::read()
{
    if (transportModel::read())
    {
        if
        (
            nuModel1_().read(subDict(phase1Name_))
         && nuModel2_().read(subDict(phase2Name_))
        )
        {
            nuModel1_->viscosityProperties().lookup("rhoS") >> rhoM_;
            //nuModel1_->viscosityProperties().lookup("rhoL") >> rhoL_;
            nuModel2_->viscosityProperties().lookup("rhoG") >> rhoG_;

            nuModel1_->viscosityProperties().lookup("cpS") >> cpS_;
            nuModel1_->viscosityProperties().lookup("cpL") >> cpL_;
            nuModel2_->viscosityProperties().lookup("cpG") >> cpG_;
            
            nuModel1_->viscosityProperties().lookup("kS") >> kS_;
            nuModel1_->viscosityProperties().lookup("kL") >> kL_;
            nuModel2_->viscosityProperties().lookup("kG") >> kG_;
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// ************************************************************************* //
